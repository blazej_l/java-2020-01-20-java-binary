int number = 14;

if (number >= 0 && number <=15) {
	int[] bin = new int[4];
	int i = 0;
	for (i = 3; i >= 0; i--) {
		bin[i] = number % 2;
		number /= 2;
	}
	for (int j = 0; j < bin.length; j++) {
		System.out.print(bin[j]);
	}
	System.out.println(" ");
} else {
	System.out.println("The given number is out of range");
}

/exit
	
